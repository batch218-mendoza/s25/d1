
//[SECTION] JSON OBJECTS

/*
	-JSON stands for Javascript object notation
	- A common use of JSON is to read data from a web server, and display the data in a web page
	- features of JSON:
		- it is a lightweight data-interchange format
		- it is easy to read and write
		- it is easy for machines to parse and generate

*/

//JSON Objects
// -JSON also use the "key/value pairs" just like the object properties in javascript
// -"key/property" names requires to be enclosed with double quotes.
/*
Syntax:
{
	"propertyA" : "valueA",
	"propertyB" : "valueb"
}

*/

// example of JSON object

/*{
	"city": "Quezon City",
	"province" : "Metro Manila",
	"country" : "Philippines"
}
*/


// Arrays of JSON object
/*
"cities" : [
{

}
]*/
// Javascript Array of objects
let batchArr = [
{
	batchName: "Batch 218",
	schedule: "Part Time"
},

{
	batchName: "Batch 219",
	schedule: "Full Name"
}
]
console.log(batchArr);

// the "stringify" method is used to convert Javascript objects into a string
console.log("Result of stringify method: ");
console.log(JSON.stringify(batchArr));


let data = JSON.stringify({
	name: "John",
	age: 31,
	address: {
		city: "Manila",
		country: "Philippines"
	}
})

console.log(data);
//global variables
/*let firstName = prompt("Enter your first name: ");
let lastName = prompt("Enter your last name: ");
let email = prompt("Enter your email: ");
let password = prompt("Enter your password:");

let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	email : email,
	password: password
})
console.log(otherData);*/

// [SECTION] Converting Stringified JSON intro Javascript Objects

let batchesJSON = `[
	{
		"batchName" : "Batch 218",
		"schedule" 	: "Part time"
	},
	{
		"batchName" : "Batch 219",
		"schedule" 	: "Full Time"
	}
]`

console.log("batchesJSON content: ")
console.log(batchesJSON);

//JSON.parse method converts JSON objects into javascript object
console.log("Result from parse method:");
console.log(JSON.parse(batchesJSON));


let parseBatches = JSON.parse(batchesJSON);
console.log(parseBatches);
console.log(parseBatches[0].batchName);

let stringifiedObject = `{
	"name" : "John",
	"age"  : 31,
	"address" : {
		"city" : "Manila",
		"country" : "Philippines"
	}




}`
console.log(stringifiedObject);
stringToObject = JSON.parse(stringifiedObject)
console.log(stringToObject);